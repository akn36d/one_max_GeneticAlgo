/* OneMax_EvoAlgo_test.cpp : Defines the entry point for the console application.
-The program applies Genetic algorithm to find a solution to the One-max problem 
for a 16 bit long binary number. 
-Since this is the one-max problem we will take the gene length to be 1
-We will use the Roulette based selection rule to select chromosomes for crossover
-We will maintain the population size to POP_SIZE throughout the program

Ref : 1. http://www.ai-junkie.com/ga/intro/gat3.html
	  
Psuedocode:
	START
	Generate the initial population
	Compute fitness
	REPEAT
		Selection based on the fitness value
		Crossover on the selected chromosomes
		Mutation of chromosomes with fitness value lesser than that of the chromosomes selected in the previous step
		Compute fitness
	UNTIL population has converged
	STOP
*/

#include "stdafx.h"

#include <iostream>
#include <string>
#include <vector>

#include <stdlib.h>
#include <time.h>

#define CROSSOVER_RATE              0.7
#define MUTATION_RATE               0.001
#define POP_SIZE                    50
#define CHROMO_LENGTH               16  // maximum possible fitness value for the one max problem
#define GENE_LENGTH                 1 
#define MAX_ALLOWABLE_GENERATIONS   100

// Returns a float between 0 & 1 
#define RANDOM_NUM                  (  (float)(rand() % 10) / (10.0f))


//----------------------------------------------------------------------------------------
//
//	define a data structure which will define a chromosome
//
//----------------------------------------------------------------------------------------
struct chromo_typ {
	std::string bits; // the binary data is held in a string

		int fitness;

		// constructors for the chromosome struct
		chromo_typ(): bits(""), fitness(0) {}
		chromo_typ(std::string bts, float ftnss) : bits(bts), fitness(ftnss) {}
};


////////////////////////////////////////Function Prototypes////////////////////////////////////////
std::string getRandomBits(int length);
int assignFitness(std::string chromo_bits, int target_ftns);
std::vector<chromo_typ> getInitPopulation();
void printChromosome(const chromo_typ& chromo);
std::string selectionRoulette(int total_fitness, std::vector<chromo_typ> chromoPopulation);
void crossover(std::string& offspring1, std::string& offspring2);
void mutate(std::string& chromo);

int main()
{
	bool runGA(true);
	while (runGA) 
	{
		srand((int)time(NULL)); // initialize the random number generator with some real value (current time is used here)
		std::vector<chromo_typ> the_population = getInitPopulation();

		int GenerationsToFindSolution = 0; // counter for number of generations to reach solution
		bool solFound = false; // flag set to true if solution is found

		// Create a GA loop to find soln
		while (!solFound)
		{
			// 0. PRELIMINARY STEPS-----------------------------------------------
			// this is used in the Roulette based sampling
			int Total_Fitness(0);

			// test and update the fitness values in the population
			for (chromo_typ& chr_obj : the_population)
			{
				chr_obj.fitness = assignFitness(chr_obj.bits, CHROMO_LENGTH);
				Total_Fitness += chr_obj.fitness;
			}

			// Check to see if we found the solution
			for (chromo_typ& chr_obj : the_population)
			{
				if (chr_obj.fitness > CHROMO_LENGTH)  // solution will have a very high fitness value, much greater than chromosome length
				{
					std::cout << "\n Solution found in " << GenerationsToFindSolution << " generations!" << std::endl;
					solFound = true;
					printChromosome(chr_obj);
					break;
				}
			}


			// define a temporary storage for the new population
			std::vector<chromo_typ> temp_population(POP_SIZE);

			int popCntr(0); // counter for generation of new population

			// Loop to generate new population  (we create a whole new population)
			while (popCntr < POP_SIZE)
			{
				// 1. SELECTION (Roulette based)------------------------------------------
				std::string offspring1 = selectionRoulette(Total_Fitness, the_population);
				std::string offspring2 = selectionRoulette(Total_Fitness, the_population);

				// 2. CROSSOVER
				crossover(offspring1, offspring2);

				// 3. MUTATION
				mutate(offspring1);
				mutate(offspring2);

				chromo_typ chromo_obj1;
				chromo_obj1.bits = offspring1;
				chromo_obj1.fitness = 0;

				chromo_typ chromo_obj2;
				chromo_obj2.bits = offspring2;
				chromo_obj2.fitness = 0;

				temp_population.at(popCntr++) = chromo_obj1;
				temp_population.at(popCntr++) = chromo_obj2;
			}

			// Number of generations over
			std::cout << "\nthe current gen is : " << GenerationsToFindSolution++;

			if (GenerationsToFindSolution > MAX_ALLOWABLE_GENERATIONS)
			{
				std::cout << "\nNo solution found this generation" << std::endl;
				break;
			}

			// setting the population to the new population
			the_population = temp_population;
		}
	
		char optionRunGA;
		std::cout << "\nRUN THE GA AGAIN ? (Y/N) " << std::endl;
		std::cin >> optionRunGA;
		runGA = (optionRunGA == 'Y' | optionRunGA == 'y') ? true : false;
	}

	short tmpVar;
	std::cin >> tmpVar;
	
	std::cout << "Done";
    return 0;
}


// Function genertes a string of 0s and 1s of the desired length
std::string getRandomBits(int length)
{
	std::string bits;
	for (int i = 0; i < length; i++)
	{	
		float randnum = RANDOM_NUM;
		if (RANDOM_NUM > 0.5f)
			bits += "1";
		else
			bits += "0";
	}
	return bits;
}

// Function to calculate the fitness value for a chromosome
int assignFitness(std::string chromo_val, int target_ftns)
{
	int fitness(0);
	for (const auto& i : chromo_val)
	{
		(i=='1') ? fitness++: fitness;
	}

	// if the fitness is the solution fitness then we assign it a really high number
	fitness = (fitness == CHROMO_LENGTH) ? 999: fitness;

	return fitness;
}

// Function to get the init population
std::vector<chromo_typ> getInitPopulation()
{
	int pop_size = POP_SIZE;
	std::vector<chromo_typ> population;
 	for (int i = 0 ; i < pop_size; i++)
	{
		chromo_typ chromo_obj;
		std::string valBits = getRandomBits(CHROMO_LENGTH);
		int fitness = 0;

		if (std::string(CHROMO_LENGTH, '1') != valBits) // if the generated chromosome is not the solution then push it to the population vector
		{
			chromo_obj.bits = valBits;
			chromo_obj.fitness = fitness;

			population.push_back(chromo_obj);
		}
		else // if it is the solution then discard it and then generate a new string 
		{
			i--;
		}
	}
	return population;
}

// Function to print out Chromosome bits
void printChromosome(const chromo_typ& chromo)
{
	std::cout << chromo.bits << std::endl;
}

// Function to do Roulette based sampling
std::string selectionRoulette(int total_fitness, std::vector<chromo_typ> chromoPopulation)
{
	// Generate a random number from the total_fitness value
	float sliceOfPie = static_cast<float>(RANDOM_NUM * total_fitness);

	// var to store the fitness so far (we use this to figure out "where the ball lands" )
	int fitnessSoFar(0);
	
	// Spinning the roulette wheel ("Figuratively speaking")
	for (const chromo_typ& chromo_obj : chromoPopulation)
	{
		fitnessSoFar += chromo_obj.fitness;

		if (fitnessSoFar >= sliceOfPie)
		{
			return chromo_obj.bits;
		}
	}
	return "";
}


// Function to do a crossover operation on two chromosomes
void crossover(std::string& offspring1, std::string& offspring2)
{
	// Dependant on crossover rate
	if (RANDOM_NUM < CROSSOVER_RATE) 
	{
		int crossover_pt(static_cast<int>(RANDOM_NUM * (CHROMO_LENGTH - 1))); // setting point of crossover
		std::string newOffspring1(offspring1.substr(0, crossover_pt) + offspring1.substr(crossover_pt, CHROMO_LENGTH));
		std::string newOffspring2(offspring2.substr(0, crossover_pt) + offspring1.substr(crossover_pt, CHROMO_LENGTH));

		offspring1 = newOffspring1;
		offspring2 = newOffspring2;
	}
}

// Function to do mutation operation
void mutate(std::string& chromo)
{
	for (char& bitVal : chromo)
	{
		if (RANDOM_NUM < MUTATION_RATE)
		{
			bitVal = ((bitVal == '1') ? '0' : '1');
		}
	}
}